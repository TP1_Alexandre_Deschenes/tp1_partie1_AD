﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculMaths
{
    class Factorielle
    {
        public double FactorielleNombre (int nb1)
         {
             if (nb1 != 0)
             {
                 int res = 1;
                 for (int i = 2; i <= nb1; i++)
                 {
                     res = res * i;
                 }
                 return res;
             }
             else
                 return 0;
        }
    }
}
